
public class SalariedEmployee implements Employee {
	private double yearly;
	
	public SalariedEmployee(double yearly) {
		this.yearly = yearly;
	}
	
	public double getYearlyPay() {
		return this.yearly;
	}

}

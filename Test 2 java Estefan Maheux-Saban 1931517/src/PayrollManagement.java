
public class PayrollManagement {
	
	public static void main(String[] args) {
		Employee[] employees = new Employee[5];
		employees[0] = new UnionizedHourlyEmployee(32, 15.0, 1000.0);
		employees[1] = new UnionizedHourlyEmployee(32, 12.0, 1500.0);
		employees[2] = new SalariedEmployee(32000.0);
		employees[3] = new HourlyEmployee(40, 18.0);
		employees[4] = new SalariedEmployee(50000.0);
		System.out.println(getTotalExpense(employees));
	}
	
	
	public static double getTotalExpense(Employee[] employees) {
		double result = 0;
		for(Employee i: employees) {
			result += i.getYearlyPay();
		}
		return result;
	}
}

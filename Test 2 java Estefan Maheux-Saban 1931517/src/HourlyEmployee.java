
public class HourlyEmployee implements Employee {
	protected int numOfHours;
	protected double payRate;
	
	public HourlyEmployee(int numOfHours, double payRate) {
		this.numOfHours = numOfHours;
		this.payRate = payRate;
	}
	
	public double getYearlyPay() {
		return numOfHours * payRate * 52.0;
	}
	public class UnionizedHourlyEmployee extends HourlyEmployee implements Employee{ 
		private double pensionContribution;
		
		public UnionizedHourlyEmployee(int numOfHours, double payRate, double pensionContribution) {
			super(numOfHours, payRate);
			this.pensionContribution = pensionContribution;
		}
		
		public double getYearlyPay() {
			return (numOfHours * payRate * 52.0) + pensionContribution;
		}
		
	}

}

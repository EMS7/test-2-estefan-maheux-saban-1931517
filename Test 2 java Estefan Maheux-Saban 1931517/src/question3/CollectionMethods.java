package question3;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionMethods {
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
		Collection<Planet> sorted = new ArrayList<Planet>();
		
		for(Planet i: planets) {
			if(i.getOrder() == 3 || i.getOrder() == 2 || i.getOrder() == 1) {
				sorted.add(i);
			}
		}
		return sorted;
	}
}
